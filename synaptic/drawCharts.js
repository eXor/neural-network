let ctx = document.getElementById('myChart');

let chart = new Chart(ctx, {
  // The type of chart we want to create
  type: 'line',

  // The data for our dataset
  data: {
    labels: new Array(74),
    datasets: [
      {
        label: 'actual',
        borderColor: 'blue',
        data: new Array(72).fill(0)
      },
      {
        label: 'predcition',
        borderColor: 'red',
        data: new Array(74).fill(NaN),
        options: {
          spanGaps: false
        }
      }
    ]
  },

  // Configuration options go here
  options: {}
});
getActualData(
  // moment('2017-07-06 03:33:00').subtract(6, 'hours').toDate(),
  // moment('2017-07-06 03:33:00').toDate(),
);

function getActualData(from = hoursAgo(6), to = new Date()) {
  const startTime = from.getTime() / 1000 - 650;
  const endTime = to.getTime() / 1000;
  const url = `http://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=${startTime}&end=${endTime}&period=300`;
  // const url = 'day.json';

  get(url, result => {
    const resultValues = result.map(obj => obj.close);

    chart.data.labels = result.map(obj =>
      new Date(obj.date * 1000)
        .toISOString()
        .split('T')[1]
        .substr(0, 5)
    );
    chart.data.datasets[0].data = resultValues;
    getPredictionData(result, chart);
    chart.update();
  });
}

function hoursAgo(hours) {
  const date = new Date();
  date.setHours(date.getHours() - hours);
  return date;
}

function get(url, callback) {
  const httpRequest = new XMLHttpRequest();
  httpRequest.overrideMimeType('application/json');

  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        callback(JSON.parse(httpRequest.response));
      }
    }
  };

  httpRequest.open('GET', url, true);
  httpRequest.send();
}
