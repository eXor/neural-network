// Poloniex url for all data with 5M
let poloUrl = 'http://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=1431248400&end=9999999999&period=300';

// Download a page and save to disk

var http = require('follow-redirects').http;
var fs = require('fs');

// url is the location of file to download
// fileName is full path of destination
var downloadPage = function (url, fileName) {
  console.log(`Downloading ${url} to ${fileName}: `);
  http.get(url, function (response) {
    if (response.statusCode !== 200) {
      if (response) {
        console.log(response.statusCode + ' ERROR getting ' + url);
      }
      process.exit(1);
    }

    var fd = fs.openSync(fileName, 'w');
    response.on('data', function (chunk) {
      fs.write(fd, chunk, 0, chunk.length, null, function (err, written, buffer) {
        process.stdout.write('.');
        if (err) {
          console.log(err);
          process.exit(1);
        }
      });
    });

    response.on('end', function () {
      fs.closeSync(fd);
      process.exit(0);
    });
  }).on('error', function (e) {
    console.log('Got error: ' + e.message);
    process.exit(1);
  });
};

// Get command line arguments using process.argv
// argv[0] is 'node' and argv[1] is the name of the .js file

var startDate = new Date('2017-01-01').getTime();
var endDate = new Date().getTime();
var timeDeltas = {
  '5M': 300,
  '15M': 900,
  '30M': 1800,
  '1H': 3600
};
var currentDelta = '5M';
var timeDelta = timeDeltas[currentDelta];
downloadPage(
  `http://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=${startDate/1000}&end=${endDate/1000}&period=${timeDelta}`,
  `poloniex.${startDate}-${new Date(endDate)}.${currentDelta}.json`
);
// downloadPage(process.argv[2], process.argv[3]);
