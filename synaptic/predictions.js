function getPredictionData(actualData, chart) {
  chart = chart.config.data;
  let chartdata = chart.datasets[1].data;
  chartdata[chartdata.length - 1] = actualData[actualData.length - 1].close;

  Array(30).fill(0).forEach((value, index, array) => {
    chart.labels.push('next');
    let lastPrice = actualData[actualData.length - 1].weightedAverage;
    let newPoint = getNewPrice(
      lastPrice,
      generateNextPoint(actualData)
    );
    chartdata.push(newPoint);
    actualData.push({ weightedAverage: newPoint });
  });
}

function getNewPrice(lastPrice, output) {
  // (end-begin/begin)/2 + 0.5 = normalized
  // (0,4999 - 0,5)*2*2300+2300 = 2295,4
  // (output - 0,5)*2*lastPrice+lastPrice
  return (output - 0.5) * 10 * lastPrice + lastPrice;
}
/**
 *  @param poloniexData {poloniex data}
 */
function generateNextPoint(poloniexData, candlesticksPerPeriod = 72) {
  poloniexData = poloniexData.slice(-73);

  let normalizedData = poloniexData
    .map(({ weightedAverage }, idx, array) => {
      if (idx < candlesticksPerPeriod) {
        return getNormalized(
          weightedAverage,
          array[idx + 1].weightedAverage
        );
      } else {
        return 0;
      }
    })
    .filter(x => x !== 0);
  let prediction = window.network.activate(normalizedData);
  return prediction;
}

function getNormalized(begin, end) {
  var delta = end - begin;
  var percentage = delta / begin;
  var normalizedValue = percentage / 2 + 0.5;

  // console.log('getNormalized', begin, end, normalizedValue);
  return normalizedValue;
}
