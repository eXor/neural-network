Neural network to predict the direction of the price of a cryptocurrency

# Files

- getdata.js: download data to a file from poloniex
- index.html: to show a chart with data
- mynetwork.js: runner for neural network
- neuralnet.js: contains the neuralnetwork and the trainer
- poloniexdata-1-1-5-7.json: raw data from poloniex
- poloniex.1483228800000-05 2017 11h 16m 13s.5m.json: same raw data from poloniex
- result.network.json: stored neural network from neuralnet.js
- training.data.json: normalized data from poloniex adapted to work with neural network

# What does this all do?

1. download poloniex data using `getdata.js`.
2. normalize this data (in `neuralnet.js`) to be suitable for the neural network.
At this moment it's using only the `weightedaverage` from each datapoint.
3. use that data to train the network (`neuralnet.js:85`)
4. store the result of the network in a file (`neuralnet.js:96`)
5. use this network in a browser to generate a chart (still not very much thought out)
