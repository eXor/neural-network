// setup neural net
let file = require('./poloniex-data-1-1-5-7.json').slice(30000, 53391);
let { Architect, Trainer } = require('synaptic');
let assert = require('assert');
let fs = require('fs');

console.log(
  `starting with ${file.length} candlesticks, which represents ${5 *
    file.length} minutes, about ${Math.round(5 * file.length / 60 / 24 * 100) /
    100} days`
);
// 6 uur
// 6*60/5 = 72 candlesticks
// per candlestick: 6 datapoints/neurons
let candlesticksPerPeriod = 6 * 60 / 5;
let datapointsPerCandle = 1; // one datapoint weightedAverage
let network = new Architect.LSTM(
  candlesticksPerPeriod * datapointsPerCandle,
  candlesticksPerPeriod * datapointsPerCandle / 2,
  1
);

let outputMap = file.map(({ weightedAverage }) => {
  return { weightedAverage };
});

// console.log('outputMap', outputMap);

// ((2541-2544)/2544)/2+0,5 = 0,4994103774
// ((2530-2520)/2520)/2+0,5 = 0,501984127
function getNormalized(begin, end) {
// (end-begin/begin)/2 + 0.5 = normalized
  var delta = end - begin;
  var percentage = delta / begin;
  var normalizedValue = percentage / 2 + 0.5;

  // console.log('getNormalized', begin, end, normalizedValue);
  return normalizedValue;
}

assert.equal(0.5, getNormalized(10, 10));
assert.equal(0.4, getNormalized(10, 8));
assert.equal(0.55, getNormalized(10, 11));

let trainingData = outputMap
  .map(({ weightedAverage }, idx, array) => {
    // console.log('wheightedAverage', weightedAverage);
    let input = [];
    for (var index = 0; index < candlesticksPerPeriod; index++) {
      // console.log('array index', array[index]);
      var currWeightedAverage = array[index];
      var nextWeightedAverage = array[index + 1];
      input.push(
        getNormalized(
          currWeightedAverage.weightedAverage,
          nextWeightedAverage.weightedAverage
        )
      );
    }

    let output = [];
    if (
      !(
        array[idx + candlesticksPerPeriod] &&
        array[idx + candlesticksPerPeriod + 1]
      )
    ) {
      return 0;
    } else {
      output = [
        getNormalized(
          array[idx + candlesticksPerPeriod].weightedAverage,
          array[idx + candlesticksPerPeriod + 1].weightedAverage
        )
      ];
    }
    // console.log('map input output', { input, output });
    return { input, output };
  })
  .filter(x => x !== 0);

// console.log('trainingData', trainingData);
// console.log(JSON.stringify(trainingData, null, '\t'));

// training
let trainer = new Trainer(network);
let result = trainer.train(trainingData, {
  rate: 0.1,
  iterations: 10,
  error: 0.005,
  shuffle: true,
  log: 1,
  cost: Trainer.cost.MSE
});

console.log('done', result, new Date().toLocaleTimeString());
fs.writeFileSync('result.network.json', JSON.stringify(network.toJSON(), null, '\t'), { encoding: 'utf8' });
